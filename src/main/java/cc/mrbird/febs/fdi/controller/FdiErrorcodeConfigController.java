package cc.mrbird.febs.fdi.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.fdi.entity.FdiErrorcodeConfig;
import cc.mrbird.febs.fdi.service.IFdiErrorcodeConfigService;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Shinn
 * @date 2019-10-30 14:22:05
 */
@Slf4j
@Validated
@RestController
@RequestMapping("fdiErrorcodeConfig")
public class FdiErrorcodeConfigController extends BaseController {

    @Autowired
    private IFdiErrorcodeConfigService fdiErrorcodeConfigService;
/**
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdiErrorcodeConfig")
    public String fdiErrorcodeConfigIndex(){
        return FebsUtil.view("fdiErrorcodeConfig/fdiErrorcodeConfig");
    }
*/
    @GetMapping
    @ResponseBody
    @RequiresPermissions("fdierrorcodeconfig:view")
    public FebsResponse getAllFdiErrorcodeConfigs(FdiErrorcodeConfig fdiErrorcodeConfig) {
        return new FebsResponse().success().data(fdiErrorcodeConfigService.findFdiErrorcodeConfigs(fdiErrorcodeConfig));
    }
    
    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("fdierrorcodeconfig:view")
    public FebsResponse fdiErrorcodeConfigList(QueryRequest request, FdiErrorcodeConfig fdiErrorcodeConfig) {
        Map<String, Object> dataTable = getDataTable(this.fdiErrorcodeConfigService.findFdiErrorcodeConfigs(request, fdiErrorcodeConfig));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增FdiErrorcodeConfig", exceptionMessage = "新增FdiErrorcodeConfig失败")
    @PostMapping
    @ResponseBody
    @RequiresPermissions("fdiErrorcodeconfig:add")
    public FebsResponse addFdiErrorcodeConfig(@Valid FdiErrorcodeConfig fdiErrorcodeConfig) {
        this.fdiErrorcodeConfigService.createFdiErrorcodeConfig(fdiErrorcodeConfig);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除FdiErrorcodeConfig", exceptionMessage = "删除FdiErrorcodeConfig失败")
    @GetMapping("delete/{ids}")
    @ResponseBody
    @RequiresPermissions("fdierrorcodeconfig:delete")
    public FebsResponse deleteFdiErrorcodeConfig(@NotNull(message="{required}")  @PathVariable String ids) {
    	//FdiErrorcodeConfig fdiErrorcodeConfig=new FdiErrorcodeConfig();
    	//fdiErrorcodeConfig.setId(id);
    	String[] fids = ids.split(StringPool.COMMA);
        this.fdiErrorcodeConfigService.deleteFdiErrorcodeConfig(fids);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改FdiErrorcodeConfig", exceptionMessage = "修改FdiErrorcodeConfig失败")
    @PostMapping("update")
    @ResponseBody
    @RequiresPermissions("fdierrorcodeconfig:update")
    public FebsResponse updateFdiErrorcodeConfig(FdiErrorcodeConfig fdiErrorcodeConfig) {
        this.fdiErrorcodeConfigService.updateFdiErrorcodeConfig(fdiErrorcodeConfig);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改FdiErrorcodeConfig", exceptionMessage = "导出Excel失败")
    @PostMapping("excel")
    @ResponseBody
    @RequiresPermissions("fdierrorcodeconfig:export")
    public void export(QueryRequest queryRequest, FdiErrorcodeConfig fdiErrorcodeConfig, HttpServletResponse response) {
        List<FdiErrorcodeConfig> fdiErrorcodeConfigs = this.fdiErrorcodeConfigService.findFdiErrorcodeConfigs(queryRequest, fdiErrorcodeConfig).getRecords();
        ExcelKit.$Export(FdiErrorcodeConfig.class, response).downXlsx(fdiErrorcodeConfigs, false);
    }
}
