package cc.mrbird.febs.fdi.controller;

import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.exception.RedisConnectException;
import cc.mrbird.febs.common.utils.DateUtil;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.monitor.entity.RedisInfo;
import cc.mrbird.febs.monitor.service.IRedisService;

import java.util.List;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/** 
 * @author MrBird
 */
@Controller("fdiView")
public class ViewController extends BaseController{


    /**
     * errorcodeconfig 
     * @return
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorcodeConfig")
    @RequiresPermissions("fdiErrorcodeConfig:view")
    public String errorcodeConfigView() {
        return FebsUtil.view("fdi/errorcode/index");
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorcodeConfig/add")
    @RequiresPermissions("fdiErrorcodeConfig:add")
    public String errorcodeConfigAdd() {
    	return FebsUtil.view("fdi/errorcode/add");
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorcodeConfig/delete/{ids}")
    @RequiresPermissions("fdiErrorcodeConfig:delete")
    public String errorcodeConfigDelete(@PathVariable String ids) {
    	return FebsUtil.view("fdi/errorcode/delete");
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorcodeConfig/update")
    @RequiresPermissions("fdiErrorcodeConfig:update")
    public String errorcodeConfigUpdate() {
    	return FebsUtil.view("fdi/errorcode/update");
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorcodeConfig/excel")
    @RequiresPermissions("fdiErrorcodeConfig:export")
    public String errorcodeConfigExport() {
    	return null;
    }
    
	
	
    /**
     * fdiErrorrecord
     */
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorrecordConfig")
    @RequiresPermissions("fdiErrorrecord:view")
    public String fdiErrorrecordIndex(){
        return FebsUtil.view("fdi/errorrecord/index");
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorrecordConfig/add")
    @RequiresPermissions("fdiErrorrecord:add")
    public String fdiErrorrecordAdd(){
        return FebsUtil.view("fdi/errorrecord/add");
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorrecordConfig/delete/{ids}")
    @RequiresPermissions("fdiErrorrecord:delete")
    public String fdiErrorrecordIndex(@PathVariable String ids){
        return null;
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorrecordConfig/update")
    @RequiresPermissions("fdiErrorrecord:update")
    public String fdiErrorrecordUpdate(){
        return FebsUtil.view("fdi/errorrecord/update");
    }
    
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdi/errorrecordConfig/excel")
    @RequiresPermissions("fdiErrorrecord:export")
    public String fdiErrorrecordExport(){
        return null;
    }
}
