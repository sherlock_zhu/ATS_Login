package cc.mrbird.febs.fdi.controller;

import cc.mrbird.febs.common.annotation.ControllerEndpoint;
import cc.mrbird.febs.common.utils.FebsUtil;
import cc.mrbird.febs.common.entity.FebsConstant;
import cc.mrbird.febs.common.controller.BaseController;
import cc.mrbird.febs.common.entity.FebsResponse;
import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.fdi.entity.FdiErrorrecord;
import cc.mrbird.febs.fdi.service.IFdiErrorrecordService;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import java.util.List;
import java.util.Map;

/**
 *  Controller
 *
 * @author Shinn
 * @date 2019-11-05 15:21:30
 */
@Slf4j
@Validated
@RestController
@RequestMapping("fdiErrorrecord")
public class FdiErrorrecordController extends BaseController {

    @Autowired
    private IFdiErrorrecordService fdiErrorrecordService;
/*
    @GetMapping(FebsConstant.VIEW_PREFIX + "fdiErrorrecord")
    public String fdiErrorrecordIndex(){
        return FebsUtil.view("fdiErrorrecord/fdiErrorrecord");
    }
*/
    @GetMapping
    @ResponseBody
    @RequiresPermissions("fdierrorrecord:view")
    public FebsResponse getAllFdiErrorrecords(FdiErrorrecord fdiErrorrecord) {
        return new FebsResponse().success().data(fdiErrorrecordService.findFdiErrorrecords(fdiErrorrecord));
    }

    @GetMapping("list")
    @ResponseBody
    @RequiresPermissions("fdierrorrecord:view")
    public FebsResponse fdiErrorrecordList(QueryRequest request, FdiErrorrecord fdiErrorrecord) {
        Map<String, Object> dataTable = getDataTable(this.fdiErrorrecordService.findFdiErrorrecords(request, fdiErrorrecord));
        return new FebsResponse().success().data(dataTable);
    }

    @ControllerEndpoint(operation = "新增FdiErrorrecord", exceptionMessage = "新增FdiErrorrecord失败")
    @PostMapping
    @ResponseBody
    @RequiresPermissions("fdierrorrecord:add")
    public FebsResponse addFdiErrorrecord(@Valid FdiErrorrecord fdiErrorrecord) {
        this.fdiErrorrecordService.createFdiErrorrecord(fdiErrorrecord);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "删除FdiErrorrecord", exceptionMessage = "删除FdiErrorrecord失败")
    @GetMapping("delete/{ids}")
    @ResponseBody
    @RequiresPermissions("fdierrorrecord:delete")
    public FebsResponse deleteFdiErrorrecord(@NotBlank(message = "{required}") @PathVariable String ids) {
    	String[] errorIds=ids.split(StringPool.COMMA);
        this.fdiErrorrecordService.deleteFdiErrorrecord(errorIds);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改FdiErrorrecord", exceptionMessage = "修改FdiErrorrecord失败")
    @PostMapping("update")
    @ResponseBody
    @RequiresPermissions("fdierrorrecord:update")
    public FebsResponse updateFdiErrorrecord(FdiErrorrecord fdiErrorrecord) {
        this.fdiErrorrecordService.updateFdiErrorrecord(fdiErrorrecord);
        return new FebsResponse().success();
    }

    @ControllerEndpoint(operation = "修改FdiErrorrecord", exceptionMessage = "导出Excel失败")
    @PostMapping("excel")
    @ResponseBody
    @RequiresPermissions("fdierrorrecord:export")
    public void export(QueryRequest queryRequest, FdiErrorrecord fdiErrorrecord, HttpServletResponse response) {
        List<FdiErrorrecord> fdiErrorrecords = this.fdiErrorrecordService.findFdiErrorrecords(queryRequest, fdiErrorrecord).getRecords();
        ExcelKit.$Export(FdiErrorrecord.class, response).downXlsx(fdiErrorrecords, false);
    }
}
