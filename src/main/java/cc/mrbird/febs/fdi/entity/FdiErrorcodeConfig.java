package cc.mrbird.febs.fdi.entity;


import lombok.Data;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Shinn
 * @date 2019-10-30 14:22:05
 */
@Data
@TableName("fdi_errorcode_config")
@KeySequence(value = "FDI_ERRORCODE_CONFIG_ID_SEQ", clazz = Long.class)
public class FdiErrorcodeConfig {

	private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @TableField("CATEGORY")
    private String category;

    /**
     * 
     */
    @TableField("DESCRIPTION")
    private String description;

    /**
     * 
     */
    @TableField("EMAIL_GROUP")
    private Long emailGroup;

    /**
     * 
     */
    @TableField("ERRORCODE")
    private Integer errorcode;
    
    @DateTimeFormat(pattern="HH:mm:ss")
    @TableField("WORKTIME_START")
    private Date worktimeStart;
    
    @DateTimeFormat(pattern="HH:mm:ss")
    @TableField("WORKTIME_END")
    private Date worktimeEnd;

    /**
     * 
     */
    @TableId(value = "ID", type = IdType.INPUT)
    private Long id;
    
    @TableField(exist = false)
    private String groupDescription;

}
