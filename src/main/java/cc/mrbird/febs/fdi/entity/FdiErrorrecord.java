package cc.mrbird.febs.fdi.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  Entity
 *
 * @author Shinn
 * @date 2019-11-05 15:21:30
 */
@Data
@TableName("fdi_errorrecord")
@KeySequence(value = "FDI_FILETYPE_RULES_ID_SEQ", clazz = Long.class)
public class FdiErrorrecord {

	private static final long serialVersionUID = 1L;
    /**
     * 
     */
    @TableId(value = "ID", type = IdType.INPUT)
    private Long id;

    /**
     * 
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 
     */
    @TableField("CONTEXT")
    private String context;

    /**
     * 
     */
    @TableField("CATEGORY")
    private String category;

    /**
     * 
     */
    @TableField("ERRORCODE")
    private Integer errorcode;

    /**
     * 
     */
    @TableField("DESCRIPTION")
    private String description;

    /**
     * 
     */
    @TableField("IS_HANDLED_FLAG")
    private Byte isHandledFlag;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    private Date createTimeFrom;
    
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    private Date createTimeTo;
}
