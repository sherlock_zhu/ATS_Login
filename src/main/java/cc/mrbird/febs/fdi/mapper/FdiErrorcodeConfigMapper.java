package cc.mrbird.febs.fdi.mapper;

import cc.mrbird.febs.fdi.entity.FdiErrorcodeConfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 *  Mapper
 *
 * @author Shinn
 * @date 2019-10-30 14:22:05
 */
public interface FdiErrorcodeConfigMapper extends BaseMapper<FdiErrorcodeConfig> {
	IPage<FdiErrorcodeConfig> finderrorCodeCofnigsPage(Page page,
			@Param("fdiErrorcodeConfig")FdiErrorcodeConfig fdiErrorcodeConfig);
	
	List<FdiErrorcodeConfig> finderrorCodeCofnigsAll(
			@Param("fdiErrorcodeConfig")FdiErrorcodeConfig fdiErrorcodeConfig);
	
	String findMailListByErrorCode(String errorCode);

}
