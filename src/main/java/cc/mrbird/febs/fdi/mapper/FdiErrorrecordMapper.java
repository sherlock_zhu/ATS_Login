package cc.mrbird.febs.fdi.mapper;

import cc.mrbird.febs.fdi.entity.FdiErrorrecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper
 *
 * @author Shinn
 * @date 2019-11-05 15:21:30
 */
public interface FdiErrorrecordMapper extends BaseMapper<FdiErrorrecord> {

}
