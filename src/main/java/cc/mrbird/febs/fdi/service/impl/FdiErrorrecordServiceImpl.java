package cc.mrbird.febs.fdi.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.fdi.entity.FdiErrorrecord;
import cc.mrbird.febs.fdi.mapper.FdiErrorrecordMapper;
import cc.mrbird.febs.fdi.service.IFdiErrorrecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 *  Service实现
 *
 * @author Shinn
 * @date 2019-11-05 15:21:30
 */
@DS("fdi")
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FdiErrorrecordServiceImpl extends ServiceImpl<FdiErrorrecordMapper, FdiErrorrecord> implements IFdiErrorrecordService {

    @Autowired
    private FdiErrorrecordMapper fdiErrorrecordMapper;

    @Override
    public IPage<FdiErrorrecord> findFdiErrorrecords(QueryRequest request, FdiErrorrecord fdiErrorrecord) {
        LambdaQueryWrapper<FdiErrorrecord> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        if (fdiErrorrecord.getErrorcode()!=null&&StringUtils.isNotBlank( fdiErrorrecord.getErrorcode().toString()))
        	queryWrapper.eq(FdiErrorrecord::getErrorcode, fdiErrorrecord.getErrorcode());
	    
	    if (fdiErrorrecord.getIsHandledFlag()!=null&& StringUtils.isNotBlank( fdiErrorrecord.getIsHandledFlag().toString()))
        	queryWrapper.eq(FdiErrorrecord::getIsHandledFlag, fdiErrorrecord.getIsHandledFlag());
	    
	    if (fdiErrorrecord.getCreateTimeFrom()!=null&&
	    		fdiErrorrecord.getCreateTimeTo()!=null&&
	    		StringUtils.isNotBlank(fdiErrorrecord.getCreateTimeFrom().toString())&&
	    		StringUtils.isNotBlank(fdiErrorrecord.getCreateTimeTo().toString())) {
            queryWrapper
                    .ge(FdiErrorrecord::getCreateTime, fdiErrorrecord.getCreateTimeFrom())
                    .le(FdiErrorrecord::getCreateTime, fdiErrorrecord.getCreateTimeTo());
        }
	    
        Page<FdiErrorrecord> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<FdiErrorrecord> findFdiErrorrecords(FdiErrorrecord fdiErrorrecord) {
	    LambdaQueryWrapper<FdiErrorrecord> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional
    public void createFdiErrorrecord(FdiErrorrecord fdiErrorrecord) {
        this.save(fdiErrorrecord);
    }

    @Override
    @Transactional
    public void updateFdiErrorrecord(FdiErrorrecord fdiErrorrecord) {
        this.saveOrUpdate(fdiErrorrecord);
    }

    @Override
    @Transactional
    public void deleteFdiErrorrecord(String[] fdiErrorrecordIds) {
        //LambdaQueryWrapper<FdiErrorrecord> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    //this.remove(wrapper);
    	List<String> list=Arrays.asList(fdiErrorrecordIds);
        this.removeByIds(list);
	}
}
