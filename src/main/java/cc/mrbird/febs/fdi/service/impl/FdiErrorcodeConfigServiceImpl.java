package cc.mrbird.febs.fdi.service.impl;

import cc.mrbird.febs.common.entity.QueryRequest;
import cc.mrbird.febs.fdi.entity.FdiErrorcodeConfig;
import cc.mrbird.febs.fdi.entity.FdiErrorrecord;
import cc.mrbird.febs.fdi.mapper.FdiErrorcodeConfigMapper;
import cc.mrbird.febs.fdi.service.IFdiErrorcodeConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 *  Service实现
 *
 * @author Shinn
 * @date 2019-10-30 14:22:05
 */
@DS("fdi")
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FdiErrorcodeConfigServiceImpl extends ServiceImpl<FdiErrorcodeConfigMapper, FdiErrorcodeConfig> implements IFdiErrorcodeConfigService {

    @Autowired
    private FdiErrorcodeConfigMapper fdiErrorcodeConfigMapper;

    @Override
    public IPage<FdiErrorcodeConfig> findFdiErrorcodeConfigs(QueryRequest request, FdiErrorcodeConfig fdiErrorcodeConfig) {
        LambdaQueryWrapper<FdiErrorcodeConfig> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
//        if (fdiErrorcodeConfig.getErrorcode()!=null&&StringUtils.isNotBlank( fdiErrorcodeConfig.getErrorcode().toString()))
//        	queryWrapper.eq(FdiErrorcodeConfig::getErrorcode, fdiErrorcodeConfig.getErrorcode());
//        
//        if (fdiErrorcodeConfig.getEmailGroup()!=null&&StringUtils.isNotBlank(fdiErrorcodeConfig.getEmailGroup().toString()))
//        	queryWrapper.eq(FdiErrorcodeConfig::getEmailGroup, fdiErrorcodeConfig.getEmailGroup());
        
        Page<FdiErrorcodeConfig> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.baseMapper.finderrorCodeCofnigsPage(page, fdiErrorcodeConfig);
        //return this.page(page, queryWrapper);
    }

    @Override
    public List<FdiErrorcodeConfig> findFdiErrorcodeConfigs(FdiErrorcodeConfig fdiErrorcodeConfig) {
	    //LambdaQueryWrapper<FdiErrorcodeConfig> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.finderrorCodeCofnigsAll(fdiErrorcodeConfig);
    }

    @Override
    @Transactional
    public void createFdiErrorcodeConfig(FdiErrorcodeConfig fdiErrorcodeConfig) {
        this.save(fdiErrorcodeConfig);
    }

    @Override
    @Transactional
    public void updateFdiErrorcodeConfig(FdiErrorcodeConfig fdiErrorcodeConfig) {
        this.saveOrUpdate(fdiErrorcodeConfig);
    }

    @Override
    @Transactional
    public void deleteFdiErrorcodeConfig(String[] ids) {
        //LambdaQueryWrapper<FdiErrorcodeConfig> wrapper = new LambdaQueryWrapper<>();
        //wrapper.setEntity(fdiErrorcodeConfig);
	    // TODO 设置删除条件
	    //this.remove(wrapper);
    	List<String> list=Arrays.asList(ids);
        this.removeByIds(list);
	}

	@Override
	public String getMailListByErrorCode(int errorCode) {
		
		return this.baseMapper.findMailListByErrorCode(String.valueOf(errorCode));
	}
}
