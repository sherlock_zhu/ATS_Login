package cc.mrbird.febs.fdi.service;

import cc.mrbird.febs.fdi.entity.FdiErrorcodeConfig;

import cc.mrbird.febs.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Shinn
 * @date 2019-10-30 14:22:05
 */
public interface IFdiErrorcodeConfigService extends IService<FdiErrorcodeConfig> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param fdiErrorcodeConfig fdiErrorcodeConfig
     * @return IPage<FdiErrorcodeConfig>
     */
    IPage<FdiErrorcodeConfig> findFdiErrorcodeConfigs(QueryRequest request, FdiErrorcodeConfig fdiErrorcodeConfig);

    /**
     * 查询（所有）
     *
     * @param fdiErrorcodeConfig fdiErrorcodeConfig
     * @return List<FdiErrorcodeConfig>
     */
    List<FdiErrorcodeConfig> findFdiErrorcodeConfigs(FdiErrorcodeConfig fdiErrorcodeConfig);

    /**
     * 新增
     *
     * @param fdiErrorcodeConfig fdiErrorcodeConfig
     */
    void createFdiErrorcodeConfig(FdiErrorcodeConfig fdiErrorcodeConfig);

    /**
     * 修改
     *
     * @param fdiErrorcodeConfig fdiErrorcodeConfig
     */
    void updateFdiErrorcodeConfig(FdiErrorcodeConfig fdiErrorcodeConfig);

    /**
     * 删除
     *
     * @param fdiErrorcodeConfig fdiErrorcodeConfig
     */
    void deleteFdiErrorcodeConfig(String[] ids);

	String getMailListByErrorCode(int i);
}
