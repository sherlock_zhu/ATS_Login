package cc.mrbird.febs.fdi.service;

import cc.mrbird.febs.fdi.entity.FdiErrorrecord;

import cc.mrbird.febs.common.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *  Service接口
 *
 * @author Shinn
 * @date 2019-11-05 15:21:30
 */
public interface IFdiErrorrecordService extends IService<FdiErrorrecord> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param fdiErrorrecord fdiErrorrecord
     * @return IPage<FdiErrorrecord>
     */
    IPage<FdiErrorrecord> findFdiErrorrecords(QueryRequest request, FdiErrorrecord fdiErrorrecord);

    /**
     * 查询（所有）
     *
     * @param fdiErrorrecord fdiErrorrecord
     * @return List<FdiErrorrecord>
     */
    List<FdiErrorrecord> findFdiErrorrecords(FdiErrorrecord fdiErrorrecord);

    /**
     * 新增
     *
     * @param fdiErrorrecord fdiErrorrecord
     */
    void createFdiErrorrecord(FdiErrorrecord fdiErrorrecord);

    /**
     * 修改
     *
     * @param fdiErrorrecord fdiErrorrecord
     */
    void updateFdiErrorrecord(FdiErrorrecord fdiErrorrecord);

    /**
     * 删除
     *
     * @param fdiErrorrecord fdiErrorrecord
     */
    void deleteFdiErrorrecord(String[] fdiErrorrecordIds);
}
