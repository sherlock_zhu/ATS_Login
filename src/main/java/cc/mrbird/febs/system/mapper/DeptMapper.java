package cc.mrbird.febs.system.mapper;

import cc.mrbird.febs.system.entity.Dept;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author MrBird
 */
public interface DeptMapper extends BaseMapper<Dept> {

	List<Dept> findDeptWithUser();
}
